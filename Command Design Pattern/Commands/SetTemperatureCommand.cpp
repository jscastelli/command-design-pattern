#include "SetTemperatureCommand.hpp"

SetTemperatureCommand::SetTemperatureCommand(Thermostat* aThermostat, double aTemperature)
{
   mThermostat = aThermostat;
   mTargetTemperature = aTemperature;
}

SetTemperatureCommand::~SetTemperatureCommand()
{
}

void SetTemperatureCommand::Execute()
{
   mPreviousTemperature = mThermostat->GetTemperature();
   mThermostat->SetTemperature(mTargetTemperature);
}

void SetTemperatureCommand::UnExecute()
{
   mThermostat->SetTemperature(mPreviousTemperature);
}