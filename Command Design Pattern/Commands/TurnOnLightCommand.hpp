#ifndef TURNONLIGHTCOMMAND_HPP
#define TURNONLIGHTCOMMAND_HPP

#include "ICommand.hpp"

#include "../Receivers/Light.hpp"

class TurnOnLightCommand : public ICommand 
{
	public:

		TurnOnLightCommand(Light* aLight);
		~TurnOnLightCommand();

		virtual void Execute();
		virtual void UnExecute();

	private:
		
		Light* mLight { nullptr };
};

#endif