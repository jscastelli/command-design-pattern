#include "TurnOnLightCommand.hpp"

TurnOnLightCommand::TurnOnLightCommand(Light* aLight)
{
   mLight = aLight;
}

TurnOnLightCommand::~TurnOnLightCommand()
{
   delete mLight;
   mLight = nullptr;
}

void TurnOnLightCommand::Execute()
{
   mLight->TurnOn();
}

void TurnOnLightCommand::UnExecute()
{
   mLight->TurnOff();
}
