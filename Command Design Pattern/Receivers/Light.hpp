#ifndef LIGHT_HPP
#define LIGHT_HPP

class Light
{
	public:
		Light() = default;
		~Light() = default;

		void TurnOn();
		void TurnOff();

		bool GetState()
		{
			return mOnOff;
		}

	private:

		bool mOnOff { false }; //True = on, False = off

};

#endif