#include "Light.hpp"

void Light::TurnOn()
{
   mOnOff = true;
}

void Light::TurnOff()
{
   mOnOff = false;
}
