#include "House.hpp"

void House::TurnOnKitchenLight()
{
   mKitchenLight.TurnOn();
}

void House::TurnOffKitchLight()
{
   mKitchenLight.TurnOff();
}

void House::TurnOnBathroomLight()
{
   mBathroomLight.TurnOn();
}

void House::TurnOffBathroomLight()
{
   mBathroomLight.TurnOff();
}

void House::SetTemperature(double aTemperature)
{
   mThermostat.SetTemperature(aTemperature);
}
