#ifndef HOUSE_HPP
#define HOUSE_HPP

#include "Light.hpp"
#include "Thermostat.hpp"

class House
{
	public:
		House() = default;
		~House() = default;

		void TurnOnKitchenLight();
		void TurnOffKitchLight();
		void TurnOnBathroomLight();
		void TurnOffBathroomLight();
		void SetTemperature(double aTemperature);

		Light mBathroomLight;
		Light mKitchenLight;
		Thermostat mThermostat;
};

#endif