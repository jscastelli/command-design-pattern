//Invoker
#include "Invoker/HouseCommandManager.hpp"

//Receiver
#include "Receivers/House.hpp"

//STD
#include <iostream>

//Helper function
void Display(House* aHouse);

int main()
{ 
   //Setup unique reciever
   House* houseReceiver = new House();

   //Create Invoker and inject commands
   HouseCommandManager* houseInvoker = new HouseCommandManager(houseReceiver);
   
   //The Invoker now has full control over the receivers and can keep track of the history
   Display(houseReceiver);
   system("pause");
   houseInvoker->TurnOnBathroomLight();
   Display(houseReceiver);
   system("pause");
   houseInvoker->SetTemperature(50.0);
   Display(houseReceiver);
   system("pause");
   houseInvoker->TurnOffBathroomLight();
   Display(houseReceiver);
   system("pause");
   houseInvoker->TurnOnKitchenLight();
   Display(houseReceiver);
   system("pause");
   houseInvoker->TurnOnBathroomLight();
   Display(houseReceiver);
   system("pause");
   houseInvoker->UndoCommand();
   Display(houseReceiver);
   system("pause");
   houseInvoker->UndoCommand();
   Display(houseReceiver);
   system("pause");
   houseInvoker->UndoCommand();
   Display(houseReceiver);
   system("pause");
   houseInvoker->RedoCommand();
   Display(houseReceiver);
   system("pause");
   houseInvoker->RedoCommand();
   Display(houseReceiver);
   system("pause");
   houseInvoker->RedoCommand();
   Display(houseReceiver);
   system("pause");
   houseInvoker->SetTemperature(75.0);
   Display(houseReceiver);
   system("pause");
   houseInvoker->SetTemperature(100.0);
   Display(houseReceiver);
   system("pause");
   houseInvoker->UndoCommand();
   Display(houseReceiver);
   system("pause");
   houseInvoker->UndoCommand();
   Display(houseReceiver);
   system("pause");
   houseInvoker->UndoCommand();
   Display(houseReceiver);
}

void Display(House* aHouse)
{
   std::cout << "==================================" << std::endl;
   std::cout << "KitchenLightOn: " << aHouse->mKitchenLight.GetState() << std::endl;
   std::cout << "BathroomLightOn: " << aHouse->mBathroomLight.GetState() << std::endl;
   std::cout << "Temperature: " << aHouse->mThermostat.GetTemperature() << std::endl;
   std::cout << "==================================" << std::endl;
}